#!/usr/bin/env python3

from setuptools import find_packages, setup
from cloud_api import __version__


with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="cloud_api",
    version=__version__,
    author="Danny Waser",
    author_email="danny@waser.tech",
    license="GPLv3+",
    description="Cloud.API: A Python library to connect to Jelastic API.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.waser.tech/wasertech/cloud.api.git",
    packages=find_packages(),
    install_requires=[
        "requests",
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Programming Language :: Python :: 3.12",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent",
        "Intended Audience :: Developers",
        "Intended Audience :: System Administrators",
        "Topic :: Software Development :: Libraries",
        "Development Status :: 4 - Beta",
    ],
    python_requires=">=3.5",
)
