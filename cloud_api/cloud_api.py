import json
import logging
import time
from functools import lru_cache
from typing import Any, Dict, List

import requests


class CloudAPIException(Exception):
    pass


class CloudAPIConnector:
    def __init__(self, platform: str, token: str):
        """
        Get all needed data to connect to a Jelastic API
        """
        self.platform = platform
        self.apidata = {"session": token, 'User-Agent': "Cloud-API"}
        self.header = {'User-Agent': "Cloud-API"}
        self.logger = logging.getLogger("CloudAPIConnector")

    def _apicall(self, uri: str, method: str = "get", data: dict = {}) -> Dict:
        """
        Lowest-level API call: that's the method that talks over the network to the Jelastic API
        """
        # Make sure we have our session in
        print("="*10)
        print("New API call :")
        print(str(method.upper()) + " to " + str(uri))
        print("With data :")
        print(data)
        print("."*10)
        self.logger.debug("_apicall {} {}, data:{}".format(method.upper(), uri, data))
        data.update(self.apidata)
        x = 0
        while x < 3:
            r = getattr(requests, method)(
                "https://{url}/1.0/{uri}".format(url=self.platform, uri=uri), data, headers=self.header
            )
            if r.status_code != requests.codes.ok:
                raise CloudAPIException(
                    "{method} to {uri} failed with HTTP code {code}".format(
                        method=method, uri=uri, code=r.status_code
                    )
                )
                
            else:
                response = r.json()
                if response["result"] != 0:
                    print(
                        "{method} to {uri} returned non-zero result: {result}".format(
                            method=method, uri=uri, result=response
                        )
                    )
                    x =+ 1
                    print("Retrying in 5 seconds...")
                    time.sleep(3)
                else:
                    self.logger.debug(" response : {}".format(response))
                    break

        print("Response :")
        print(response)
        print("-"*10)
        return response


    def _(self, function: str, **kwargs) -> Dict:
        """
        Direct API call, converting function paths into URLs; allows:
            CloudAPIConnector._('Environment.Control.GetEnvs')
        """
        self.logger.info("{fnc}({data})".format(fnc=function, data=kwargs))
        # Determine function endpoint from the two-dotted string
        uri_chunks = function.split(".")
        if len(uri_chunks) != 3:
            raise CloudAPIException(
                "Function ({fnc}) doesn't match standard Jelastic function (Group.Class.Function)".format(
                    fnc=function
                )
            )
        uri = "{grp}/{cls}/REST/{fnc}".format(
            grp=uri_chunks[0], cls=uri_chunks[1], fnc=uri_chunks[2]
        ).lower()

        return self._apicall(uri=uri, method="post", data=kwargs)


class CloudEnv:
    """
    Convenience "Env" storage class
    """

    def __init__(self, apidict: Dict) -> None:
        self.name = apidict["env"]["envName"]
        #self.id = apidict["env"]["appid"]
        self.env = apidict
        self.envGroups = set(apidict["envGroups"])
        self.nodes = self.env.get('nodes', [])

    def hasEnvGroups(self, envGroups: List[str]) -> bool:
        """
        Check that this env has a set of envGroups
        """
        return set(envGroups).issubset(self.envGroups)

    def getNodesByType(self, nodeType: str) -> List[Any]:
        """
        Get the jelastic Nodes that match given nodeType
        """
        nodes = []
        for nd in self.nodes:
            if nd.get('nodeType') == nodeType:
                nodes.append(nd)
        
        return nodes
    
    def getNodesByGroup(self, nodeGroup: str) -> List[Any]:
        """
        Get the jelastic Nodes that match given nodeGroup
        """
        nodes = []
        for nd in self.nodes:
            if nd.get('nodeGroup') == nodeGroup:
                nodes.append(nd)
        
        return nodes

    def getDockerNodes(self, dockerName: str, dockerTag: str = "latest") -> List[Any]:
        """
        Get the jelastic Nodes that match the dockerName and dockerTag
        """
        return [
            node
            for node in self.env["nodes"]
            if (
                node["nodeType"] == "docker"
                and "customitem" in node
                and node["customitem"]["dockerName"] == dockerName
                and node["customitem"]["dockerTag"] == dockerTag
            )
        ]


class CloudAPI:
    def __init__(self, platform: str, token: str) -> None:
        """
        Get all needed data to connect to a Jelastic API
        """
        self.japic = CloudAPIConnector(platform=platform, token=token)

    def test(self) -> Dict:
        """
        Test that the connection to the Jelastic API works.
        """
        return self.japic._("Users.Account.GetUserInfo")

    @property  # type: ignore
    @lru_cache(maxsize=None)
    def envs(self) -> List[CloudEnv]:
        """
        Get the dict of environments from the API
        """
        response = self.japic._("Environment.Control.GetEnvs")
        return [CloudEnv(env) for env in response["infos"]]
    
    def addVolume(self, env: CloudEnv, source_path: str, source_node: str, destination_path: str, nodeGroup: str = "cp") -> None:
        """
        Add a new volume to a node
        """
        self.japic._("Environment.File.AddMountPointByGroup", envName=env.name, nodeGroup=nodeGroup, path=destination_path, protocol="nfs", sourcePath=source_path, sourceNodeId=source_node)

    @lru_cache(maxsize=None)
    def getEnvByName(self, name: str) -> CloudEnv:
        """
        Get CloudEnv object, by name
        """
        return CloudEnv(self.japic._("Environment.Control.GetEnvInfo", envName=name))

    def getEnvsByEnvGroups(self, envGroups: List[str]) -> List[CloudEnv]:
        """
        Get environments that match the envGroups' array
        """
        return [e for e in self.envs if e.hasEnvGroups(envGroups)]  # type: ignore

    def clear_envs(self) -> None:
        type(self).envs.fget.cache_clear()  #type: ignore
        self.getEnvByName.cache_clear()

    def cloneEnv(self, sourceEnv: CloudEnv, destEnvName: str) -> CloudEnv:
        """
        Clone source environment to destEnvName.
        """
        self.japic._(
            "Environment.Control.CloneEnv",
            srcEnvName=sourceEnv.name,
            dstEnvName=destEnvName,
        )
        self.clear_envs()
        return self.getEnvByName(name=destEnvName)
 
    def createEnvironment(self, env: Dict, nodes: Dict, skipNodeEmails=True) -> None:
        """
        Create a new environment
        """
        ScriptEvalResponse = self.japic._("Environment.Control.CreateEnvironment", env=json.dumps(env), nodes=json.dumps(nodes), skipNodeEmails=skipNodeEmails)
        
        if ScriptEvalResponse['response'].get('result', False) != "0":    
            Env = CloudEnv(ScriptEvalResponse.get('response'))
            return Env
        else: 
            return self.getEnvByName(name=env.get('name'))
    
    def deleteEnv(self, env: CloudEnv, **kwargs) -> None:
        """
        Delete environment, provided that we gave it the right argument as kwarg, not just a boolean
        """
        if kwargs.get("reallySure", False):
            self.japic._("Environment.Control.DeleteEnv", envName=env.name)
            self.clear_envs()

    def attachEnvGroup(self, env: CloudEnv, envGroup: str) -> None:
        """
        Attach an EnvGroup to a CloudEnv
        """
        self.japic._(
            "Environment.Control.AttachEnvGroup", envName=env.name, envGroup=envGroup
        )
        self.clear_envs()

    def detachEnvGroup(self, env: CloudEnv, envGroup: str) -> None:
        """
        Detach an EnvGroup from a CloudEnv
        """
        self.japic._(
            "Environment.Control.DetachEnvGroup", envName=env.name, envGroup=envGroup
        )
        self.clear_envs()

    def addContainerEnvVars(
        self, env: CloudEnv, envVars: Dict, nodeGroup: str = "cp"
    ) -> None:
        """
        Add (=overwrite) container Environment Variables in given nodeGroup
        """
        self.japic._(
            "Environment.Control.AddContainerEnvVars",
            envName=env.name,
            vars=json.dumps(envVars),
            nodeGroup="cp",
        )
        self.clear_envs()

    def addContainerVolumes(self, env: CloudEnv, volumes: List, nodeGroup: str = "cp", nodeId: str = None):
        """
        Add list of files to volumes for safe keeping
        """
        if nodeId != None:
            return self.japic._(
                "Environment.Control.AddContainerVolumes",
                envName=env.name,
                nodeId=nodeId,
                volumes=volumes
            )
        else:
            return self.japic._(
                "Environment.Control.AddContainerVolumes",
                envName=env.name,
                nodeGroup=nodeGroup,
                volumes=volumes
            )

    def getContainerEnvVarsByGroup(
        self, env: CloudEnv, nodeGroup: str = "cp"
    ) -> None:
        """
        Get container Environment Variables in given nodeGroup
        """
        return self.japic._(
            "Environment.Control.GetContainerEnvVarsByGroup",
            envName=env.name,
            nodeGroup="cp",
        )

    def getExtIP(self, env: CloudEnv) -> List[str]:
        """
        Get CloudEnv external IP fo a given CloudEnv
        """
        extIPs = []
        for node in env.nodes:
            if node.get('extIPs', None):
                nd = node.get('extIPs')[0]
                extIPs.append(nd)
        return extIPs

    def jpsInstall(self, jps: str, env: CloudEnv, settings: Dict = None, nodeGroup: str = None, skipNodeEmails: bool = False) -> None:
        """
        Install jps into an Environment.
        """
        return self.japic._("Marketplace.Jps.Install", jps=jps, envName=env.name, settings=json.dumps(settings), nodeGroup=nodeGroup, skipNodeEmails=skipNodeEmails)

    def redeployContainersByGroup(
        self, env: CloudEnv, dockertag: str, nodeGroup: str = "cp"
    ) -> None:
        """
        Trigger redeployment of the containers of the given env, to the given dockertag, from the given nodeGroup
        """
        self.japic._(
            "Environment.Control.RedeployContainersByGroup",
            tag=dockertag,
            nodeGroup=nodeGroup,
            envName=env.name,
        )

    def addExtIP(self, env: CloudEnv, nodeId: int, ip_type: str = "ipv4") -> str:
        """
        Add external IP for given node ID
        """
        ScriptEvalResponse = self.japic._("Environment.Binder.AttachExtIp", envName=env.name, nodeid=nodeId, type=ip_type)
        if ScriptEvalResponse['response'].get('result', False) != "0":
            extIP = self.getExtIP(env=env)[0]
            return extIP

    def removeAllExtIPs(self, env: CloudEnv, nodeGroup: str = "bl") -> None:
        """
        Remove all external IPs from given environment to reduce cost (and break custom SSL)
        """
        for ipType in ["ipv4", "ipv6"]:
            self.japic._(
                "Environment.Binder.SetExtIpCount",
                #appid=env.id,
                nodeGroup=nodeGroup,
                type=ipType,
                count=0,
            )

    def swapExtIP(self, env: CloudEnv, source_node_id: str, target_node_id: str, ip: str) -> None:
        """
        Swap an external ip from a node to another one
        """
        self.japic._(
            "Environment.Binder.SwapExtIps",
            envName=env.name,
            sourceNodeId=source_node_id,
            targetNodeId=target_node_id,
            sourceIp=ip,
            targetIp=ip
        )

    def setBuiltInSSL(self, env: CloudEnv, sslstate: bool) -> None:
        """
        Set built-in SSL at environment level.
        """
        self.japic._(
            "Environment.Control.EditEnvSettings",
            envName=env.name,
            settings=json.dumps({"sslstate": sslstate}),
        )

    def execCmdByGroup(self, env: CloudEnv, command: str, nodeGroup: str = "cp") -> None:
        """
        Launch a specific command in a node group
        """
        stdout = self.japic._(
            "Environment.Control.ExecCmdByGroup",
            envName=env.name,
            nodeGroup=nodeGroup,
            commandList=json.dumps([{"command": command, "params": ""}]),
        )
        return stdout['responses'][0]['out']

    def setEnvDisplayName(self, env: CloudEnv, displayName: str) -> None:
        """
        Set the environment Display Name
        """
        self.japic._(
            "Environment.Control.SetEnvDisplayName",
            envName=env.name,
            displayName=displayName,
        )

    def setEnvDomain(self, env: CloudEnv, domain: str) -> None:
        """
        Set the environment Domain
        """
        self.japic._("Environment.Binder.BindExtDomain", envName=env.name, extdomain=domain)


    def setEnvGroup(self, env: CloudEnv, envGroup: str) -> None:
        """
        Set the environment Group
        """
        self.japic._("Environment.Control.SetEnvGroup", envName=env.name, envGroup=envGroup)

    def sleepEnv(self, env: CloudEnv) -> None:
        """
        Gets running environment asleep
        """
        self.japic._("Environment.Control.SleepEnv", envName=env.name)

    def startEnv(self, env: CloudEnv) -> None:
        """
        Starts environment if it is stopped
        """
        self.japic._("Environment.Control.StartEnv", envName=env.name)

    def stopEnv(self, env: CloudEnv) -> None:
        """
        Stops an environment if it is running
        """
        self.japic._("Environment.Control.StopEnv", envName=env.name)

    def restartNodes(self, env: CloudEnv, nodeGroup: str = None, nodeID: str = None, delay: int = 0, isSequential: bool = False) -> None:
        """
        Restart nodes by group or ID
        """
        self.japic._("Environment.Control.RestartNodes", envName=env.name, nodeGroup=nodeGroup, nodeId=nodeID, delay=delay, isSequential=isSequential)
    
    def createFile(self, env: CloudEnv, path: str, body: List[str], nodeType: str = None, nodeGroup: str = None, masterOnly: bool = False, nodeId: str = None) -> None:
        """
        Write to specified file
        """
        lines = ""
        for l in body:
            lines = lines + l + "\n"
        
        self.japic._("Environment.File.Write", envName=env.name, path=path, body=lines, nodeType=nodeType, nodeGroup=nodeGroup, masterOnly=masterOnly, nodeId=nodeId, isAppendMode=json.dumps(True))

    def uploadFile(self, env: CloudEnv, source_path: str, upload_path: str, nodeType: str = None, nodeGroup: str = None, masterOnly: bool = False, nodeId: str = None, overwrite: bool = False) -> None:
        """
        Upload file
        """
        self.japic._("Environment.File.Upload", envName=env.name, sourcePath=source_path, destPath=upload_path, nodeType=nodeType, nodeGroup=nodeGroup, masterOnly=masterOnly, nodeId=nodeId, overwrite=overwrite)
