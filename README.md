# Cloud.API

A Jelastic API library written in Python to manage cloud instances.

## Installation

Install `Cloud.API` with `pip`.

`pip install git+https://gitlab.com/waser-technologies/technologies/cloud.api.git`

Or build it from source.

    git clone https://gitlab.com/waser-technologies/technologies/cloud.api.git
    virtualenv env
    source env/bin/activate
    python setup.py build
    python setup.py install
    
## Usage
    
### Import Cloud.API
    from cloud_api import CloudAPI
    
### Login to your Jelastic Cloud account
Get url of your Jelastic Cloud hoster.

`API_URL = "app.jpc.infomaniak.com"`

And your authentification token.

`API_TOKEN = "4b23b4hvk25v43gv543gh5v43gh5v4"`
    
You can now initiate the connexion.

`cloud = CloudAPI(apiurl=API_URL, apitoken=API_TOKEN)`
    
### Define your environment 
    testEnv = {
        'env': {
            'displayName':"Test Environment",
            'shortdomain':"test-env",
        }, 
    'nodes': [
        
        {'count':1, 'fixedCloudlets':1, 'flexibleCloudlets':2, 'nodeType':"nginx-dockerized",},
        {'count':1, 'fixedCloudlets':1, 'flexibleCloudlets':2, 'nodeType':"apache-python",},
        
        ],
    }
    
### Create the env
    Env = cloud.createEnvironment(env=testEnv, nodes=testEnv['nodes'])
    
### Stop the env
    cloud.stopEnv(env=Env)
    
### Delete the env
    cloud.deleteEnv(env=Env)

## Thanks

Thanks to our friends from liip.ch for providing the base code.
